#title:        Dockerfile
#description:  Dockerfile of GPU-JupyterLab with Tensorflow for Bull Supercomputer
#organization: Kajaani University of Applied Sciences (KAMK)
#project:      HPC4RDI
#author:       Jukka Jurvansuu <jukka.jurv@nsuu.fi>
#created:      2021-09-29
#modified:     2021-09-29
#version:      1.0
#image:        kamkhub/bull-gpu-jupyter-tensorflow
#=========================================================================================

# GPU-Jupyter
# Ref: https://github.com/iot-salzburg/gpu-jupyter/
# Images: https://hub.docker.com/r/cschranz/gpu-jupyter/tags?page=1&ordering=last_updated
# Base Images for CUDA: https://hub.docker.com/r/nvidia/cuda

# Using CUDA base image: cuda:10.1-cudnn7-runtime-ubuntu18.04
FROM cschranz/gpu-jupyter:v1.4_cuda-10.1_ubuntu-18.04

# Switch to user
USER root

RUN apt-get update

# Install useful applications
RUN apt-get install -y --no-install-recommends \
    grep

# Install required packages
RUN apt-get install -y build-essential
RUN pip install -U opencv-python

# Install PyTorch version with support for Bull NIVIDIA Telsa K40, compute capability 3.5
# Ref: https://github.com/nelson-liu/pytorch-manylinux-binaries
# Python wheel file for binaries: https://cs.stanford.edu/~nfliu/files/pytorch/whl/torch_stable.html

# You can install PyTorch from https://nelsonliu.me
# RUN pip install torch==1.8.1+cu102 -f https://nelsonliu.me/files/pytorch/whl/torch_stable.html

# Install PyTorch from binary at KAMK Hub Files
RUN pip install https://kamkhub-hpc-files.web.app/download/bull-gpu-jupyter-tensorflow/torch-1.8.1+cu102-cp38-cp38-linux_x86_64.whl

# Switch to jovyan
USER jovyan
